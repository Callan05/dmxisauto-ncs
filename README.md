# Install:
wget https://bitbucket.org/Callan05/dmxisauto-ncs/raw/3ff94259aa2a3ace6fc672250cf1aa882bb248ea/scripts/buildenv.sh && sudo ./buildenv.sh

# Questions? #
* How often are we looking at an audio sample?
	1.	1 second?
	2.	2 seconds?
	3.	4 seconds?

# IDEAS #

## Pick up the song based on the track input ##
This one might be pretty easy if we have all of the tracks available.
May not even need to use NCS

## Pick up the BPM from click track ##


## Pick up BPM from the drums ##
Currently working OK in javascript, but can NCS improve this?

## Classify current drumming into 1 of 10 categories using audio input ##
	1. Between Songs
	2. Drum not playing
	3. Drums keeping light time - hihats perhaps
	4. Light-Medium
	5. Medium playing or a bit laid back
	6. Medium Rock playing
	7. Medium-Heavy
	8. Heavy Playing
	9. Heavier playing
	10. Drums going bat-shit crazy

# What inputs do we have available? #
## From mixer: ##
*	Full mix
*	Drum mix
*	Kick & Snare submix

# Other
*	Laptop mic
* 	Tablet mic
*	Direct input (touch screen etc.)
*	MIDI Pedal
