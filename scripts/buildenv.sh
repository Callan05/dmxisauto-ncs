if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Some settings to check:
repo="https://Callan05@bitbucket.org/Callan05/dmxisauto-ncs.git"
installroot="/opt"



# Let's do this
apt-get update
apt-get -y upgrade

apt-get install python3.5

cd $installroot
git clone $repo
chmod 775 dmxisauto-ncs


